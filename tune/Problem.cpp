#include "tune/Problem.hpp"

#include "SHO/Assert.hpp"
#include "tune/Error.hpp"

#include <fstream>
#include <optional>

namespace tune {

namespace {

std::pair<std::string_view, std::string_view>
split_and_trim_colon_tsplib_line(std::string_view line) noexcept;

std::string_view trim(std::string_view string_to_trim) noexcept;

}

namespace binary_vector {

double OneMaxProblem::Evaluator::evaluate(const Solution& solution)
{
    return std::accumulate(solution.begin(), solution.end(), 0.0);
}

}

namespace floating_point {

SquareProblem::Evaluator::Evaluator(SHO::RealFunctionDomain domain) noexcept
        : domain(domain)
{}

double SquareProblem::Evaluator::evaluate(const Solution& solution)
{
    SHO_ASSERT_FMT(domain.contains(solution),
                   "Domain {{{}, {}}} does not contains solution {}.",
                   domain.min,
                   domain.max,
                   solution);
    return -solution * solution;
}

SchafferProblem::Fitness
SchafferProblem::Evaluator::evaluate(const Solution& solution)
{
    return {-std::pow(solution, 2), -std::pow(solution - 2, 2)};
}

}

namespace permutation_vector {

TravelingSalesmanProblem::DistanceMatrix::DistanceMatrix(
    std::size_t n_nodes) noexcept
        : n_nodes{n_nodes},
          data(n_nodes * n_nodes, 0) // Avoid initializer list constructor.
{}

TravelingSalesmanProblem::DistanceMatrix::DistanceMatrix(
    MatrixInitialierList matrix) noexcept
        : n_nodes{matrix.size()}
{
    [[maybe_unused]] const auto n_rows{matrix.size()};
    std::size_t row_i{0};
    data.reserve(n_nodes * n_nodes);
    for (const auto& row : matrix) {
        [[maybe_unused]] const auto n_columns = row.size();
        SHO_ASSERT_FMT(n_columns == n_rows,
                       "Matrix must be quadratic, but row {} has {} elements "
                       "and {} were expected.",
                       row_i,
                       n_columns,
                       n_rows);
        std::size_t col_i{0};
        for (const auto& el : row) {
            SHO_ASSERT_FMT(row_i != col_i || el == 0,
                           "Diagonal elements must be zero, but element at "
                           "({}, {}) is set to {}.",
                           row_i,
                           col_i,
                           el);
            col_i++;
            data.push_back(el);
        }
        row_i++;
    }
}

void TravelingSalesmanProblem::DistanceMatrix::set_distance(
    std::size_t start,
    std::size_t destination,
    unsigned distance,
    bool symmetric) noexcept
{
    SHO_ASSERT_FMT(start < n_nodes,
                   "Start ({}) must be smaller than number of nodes ({}).",
                   start,
                   n_nodes);
    SHO_ASSERT_FMT(
        destination < n_nodes,
        "Destination ({}) must be smaller than number of nodes ({}).",
        destination,
        n_nodes);
    SHO_ASSERT_FMT(start != destination || distance == 0,
                   "If start and destination are equal ({}), distance must be "
                   "set to zero, but {} was provided.",
                   start,
                   distance);
    data[start * n_nodes + destination] = distance;
    if (symmetric) { data[destination * n_nodes + start] = distance; }
}

unsigned TravelingSalesmanProblem::DistanceMatrix::get_distance(
    std::size_t start, std::size_t destination) const noexcept
{
    SHO_ASSERT_FMT(start < n_nodes,
                   "Start ({}) must be smaller than number of nodes ({}).",
                   start,
                   n_nodes);
    SHO_ASSERT_FMT(
        destination < n_nodes,
        "Destination ({}) must be smaller than number of nodes ({}).",
        destination,
        n_nodes);
    SHO_ASSERT_FMT(data[start * n_nodes + destination] != 0 ||
                       start == destination,
                   "Distance for start {} and destination {} not set.",
                   start,
                   destination);
    return data[start * n_nodes + destination];
}

std::size_t
TravelingSalesmanProblem::DistanceMatrix::get_number_of_nodes() const noexcept
{
    return n_nodes;
}

auto TravelingSalesmanProblem::DistanceMatrix::load_from_tsplib_file(
    const std::filesystem::path& filepath) -> DistanceMatrix
{
    std::ifstream file{filepath};
    if (!file) { throw FileOpenError{filepath}; }

    std::string line;
    std::optional<DistanceMatrix> distance_matrix;
    std::optional<std::size_t> dimension;
    while (file) {
        std::getline(file, line);
        if (line.find("TYPE") == 0) {
            auto [tag, type]{split_and_trim_colon_tsplib_line(line)};
            if (type != "TSP") {
                throw TsplibUnsupportedEntryError{
                    filepath, "TYPE", std::string{type}, {"TSP"}};
            }
        } else if (line.find("DIMENSION") == 0) {
            auto [tag, dimension_str]{split_and_trim_colon_tsplib_line(line)};
            std::istringstream dimension_stream{std::string{dimension_str}};
            unsigned d;
            dimension_stream >> d;
            dimension = d;
        } else if (line.find("EDGE_WEIGHT_TYPE") == 0) {
            auto [tag, ewt]{split_and_trim_colon_tsplib_line(line)};
            if (ewt != "EXPLICIT") {
                throw TsplibUnsupportedEntryError{filepath,
                                                  "EDGE_WEIGHT_TYPE",
                                                  std::string{ewt},
                                                  {"EXPLICIT"}};
            }
        } else if (line.find("EDGE_WEIGHT_FORMAT") == 0) {
            auto [tag, ewf]{split_and_trim_colon_tsplib_line(line)};
            if (ewf != "FULL_MATRIX") {
                throw TsplibUnsupportedEntryError{filepath,
                                                  "EDGE_WEIGHT_FORMAT",
                                                  std::string{ewf},
                                                  {"FULL_MATRIX"}};
            }
        } else if (line.find("EDGE_WEIGHT_SECTION") == 0) {
            if (!dimension) { throw TsplibMissingDimensionError{filepath}; }
            distance_matrix = DistanceMatrix{*dimension};
            for (std::size_t i{0}; i < *dimension; i++) {
                for (std::size_t j{0}; j < *dimension; j++) {
                    unsigned distance;
                    file >> distance;
                    if (!file) {
                        throw TsplibInvalidEdgeWeightError{filepath, i, j};
                    }
                    distance_matrix->set_distance(i, j, distance);
                }
            }
        }
    }
    SHO_ASSERT(distance_matrix, "Distance matrix not created.");
    return *distance_matrix;
}

TravelingSalesmanProblem::Evaluator::Evaluator(
    DistanceMatrix distance_matrix) noexcept
        : distance_matrix{std::move(distance_matrix)}
{}

double TravelingSalesmanProblem::Evaluator::evaluate(const Solution& solution)
{
    SHO_ASSERT_FMT(solution.size() == distance_matrix.get_number_of_nodes(),
                   "Evaluator expects solution with {} nodes, but solution "
                   "with {} nodes has been provided.",
                   distance_matrix.get_number_of_nodes(),
                   solution.size());
    double fitness{0.0};
    for (std::size_t i{0}; i < solution.size() - 1; i++) {
        fitness += distance_matrix.get_distance(solution[i], solution[i + 1]);
    }
    fitness += distance_matrix.get_distance(solution.back(), solution.front());
    return -fitness;
}

}

namespace {

std::pair<std::string_view, std::string_view>
split_and_trim_colon_tsplib_line(std::string_view line) noexcept
{
    const auto colon_i{line.find(':')};
    SHO_ASSERT_FMT(colon_i != std::string::npos, "No colon in line: {}", line);
    const std::string_view before_colon{line.data(), colon_i - 1};
    const std::string_view after_colon{line.data() + colon_i + 1,
                                       line.size() - colon_i - 1};
    return {trim(before_colon), trim(after_colon)};
}

std::string_view trim(std::string_view string_to_trim) noexcept
{
    const auto first{string_to_trim.find_first_not_of("\n\t ")};
    if (first == std::string::npos) { return std::string_view(); }
    const auto last{string_to_trim.find_last_not_of("\n\t ")};
    return std::string_view{string_to_trim.data() + first, last - first + 1};
}

}

}
