#include "tune/ExperimentParametersLoaderTOML.hpp"

namespace tune {

std::string_view
ExperimentParametersLoaderTOML::load_problem_name(const toml::table& experiment)
{
    auto name = experiment["problem"]["name"].value<std::string_view>();
    if (!name) {
        throw InvalidNodeError{
            {BasicTracePoint{"name"}, BasicTracePoint{"problem"}}};
    }
    return *name;
}

std::string_view ExperimentParametersLoaderTOML::load_algorithm_name(
    const toml::table& experiment)
{
    auto name = experiment["algorithm"]["name"].value<std::string_view>();
    if (!name) {
        throw InvalidNodeError{
            {BasicTracePoint{"name"}, BasicTracePoint{"algorithm"}}};
    }
    return *name;
}

std::vector<std::unique_ptr<TerminationCriterionParameters>>
ExperimentParametersLoaderTOML::load_termination_criteria_parameters(
    const toml::table& terminations) const
{
    std::string name;
    try {
        std::vector<std::unique_ptr<TerminationCriterionParameters>> res;
        res.reserve(terminations.size());
        for (const auto& [key, value] : terminations) {
            name          = key;
            const auto it = terminations_parameters_loaders.find(name);
            if (it == terminations_parameters_loaders.end()) {
                throw MissingLoaderError{terminations_parameters_loaders,
                                         name,
                                         "termination criteria"};
            }
            const auto loader           = it->second;
            auto termination_parameters = loader(value);
            res.push_back(std::move(termination_parameters));
        }
        return res;
    } catch (const TraceError& exc) {
        exc.push_trace_point(BasicTracePoint{name});
        throw;
    }
}

std::optional<unsigned>
ExperimentParametersLoaderTOML::load_seed(const toml::table& algorithm)
{
    if (!algorithm.contains("seed")) { return std::nullopt; }

    auto seed = algorithm["seed"].value<unsigned>();
    if (!seed) { throw InvalidNodeError{{BasicTracePoint{"seed"}}}; }
    return seed;
}

}
