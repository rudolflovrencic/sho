#include "tune/InitializerParameters.hpp"

#include "tune/Error.hpp"

#include "SHO/Random.hpp"

namespace tune {

namespace floating_point {

RandomPopulationInitializerParameters::RandomPopulationInitializerParameters(
    unsigned population_size,
    SHO::RealFunctionDomain range,
    SHO::RealFunctionDomain domain)
        : PopulationInitializerParameters{population_size}, range{range}
{
    // Domain is validated by the problem parameters object.
    SHO_ASSERT(domain.is_valid(), "Invalid domain.");

    if (!range.is_valid()) { throw InvalidRangeError{range}; }
    if (!domain.contains(range)) {
        throw IncompatibleRangeAndDomainError{range, domain};
    }
}

std::vector<Solution> RandomPopulationInitializerParameters::load() const
{
    const auto population_size = get_population_size();

    std::vector<Solution> res;
    res.reserve(population_size);

    std::uniform_real_distribution<Solution> dist(range.min, range.max);
    for (unsigned i = 0; i < population_size; i++) {
        res.push_back(dist(SHO::Random::get_generator()));
    }

    return res;
}

FixedPopulationInitializerParameters::FixedPopulationInitializerParameters(
    std::vector<Solution> solutions, SHO::RealFunctionDomain domain)
        : PopulationInitializerParameters{static_cast<unsigned>(
              solutions.size())},
          solutions{std::move(solutions)}
{
    // Domain is validated by the problem parameters object.
    SHO_ASSERT(domain.is_valid(), "Invalid domain.");

    for (std::size_t i = 0; i < this->solutions.size(); ++i) {
        const auto& solution = this->solutions[i];
        if (!domain.contains(solution)) {
            throw SolutionOutsideDomainError{solution, domain, i};
        }
    }
}

std::vector<Solution> FixedPopulationInitializerParameters::load() const
{
    return solutions;
}

}

namespace permutation_vector {

RandomPopulationInitializerParameters::RandomPopulationInitializerParameters(
    unsigned population_size, std::size_t solution_size)
        : PopulationInitializerParameters{population_size},
          solution_size{solution_size}
{}

auto RandomPopulationInitializerParameters::load() const
    -> std::vector<Solution>
{
    Solution s;
    s.reserve(solution_size);
    for (std::size_t i = 0; i < solution_size; ++i) { s.push_back(i); }

    const auto population_size = get_population_size();

    std::vector<Solution> population;
    population.reserve(population_size);
    for (unsigned i = 0; i < population_size; ++i) {
        std::shuffle(s.begin(), s.end(), SHO::Random::get_generator());
        population.push_back(s);
    }

    return population;
}

unsigned
RandomPopulationInitializerParameters::get_solution_size() const noexcept
{
    return solution_size;
}

}

}
