#include "ThreadPool.hpp"

#include <cassert>

namespace tune {

ThreadPool::ThreadPool(unsigned n_threads) : terminate_pool{false}
{
    assert(n_threads > 0);
    threads.reserve(n_threads);
    for (unsigned i = 0; i < n_threads; ++i) {
        threads.push_back(std::thread{[this]() { perform_worker_loop(); }});
    }
}

ThreadPool::~ThreadPool() noexcept
{
    terminate_pool = true;
    tasks_condition_variable.notify_all();
    for (auto& thread : threads) { thread.join(); }
}

void ThreadPool::add_task(std::function<void()> task)
{
    {
        std::unique_lock<std::mutex> lock{tasks_mutex};
        tasks.push(std::move(task));
    }
    tasks_condition_variable.notify_one();
}

unsigned ThreadPool::get_n_threads() const noexcept
{
    return threads.size();
}

void ThreadPool::perform_worker_loop()
{
    std::function<void()> task;
    while (true) {
        {
            std::unique_lock<std::mutex> lock{tasks_mutex};
            tasks_condition_variable.wait(
                lock, [this]() { return !tasks.empty() || terminate_pool; });

            if (terminate_pool) { return; }

            task = std::move(tasks.front());
            tasks.pop();
        }
        task();
    }
}

}
