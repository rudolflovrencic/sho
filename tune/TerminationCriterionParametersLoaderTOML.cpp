#include "tune/TerminationCriterionParametersLoaderTOML.hpp"
#include "tune/Error.hpp"

namespace tune {

std::unique_ptr<TerminationCriterionParameters>
load_max_running_time(const toml::node& termination)
{
    const auto time = termination.value<toml::time>();
    if (!time) { throw InvalidNodeError{}; }
    return std::make_unique<MaxRunningTimeParameters>(
        std::chrono::hours{time->hour} + std::chrono::minutes{time->minute} +
        std::chrono::seconds{time->second} +
        std::chrono::nanoseconds{time->nanosecond});
}

std::unique_ptr<TerminationCriterionParameters>
load_max_evaluations(const toml::node& termination)
{
    const auto max_eval = termination.value<unsigned long long>();
    if (!max_eval) { throw InvalidNodeError{}; }
    return std::make_unique<MaxEvaluationsParameters>(*max_eval);
}

std::unique_ptr<TerminationCriterionParameters>
load_max_iterations(const toml::node& termination)
{
    const auto max_iter = termination.value<unsigned long long>();
    if (!max_iter) { throw InvalidNodeError{}; }
    return std::make_unique<MaxIterationsParameters>(*max_iter);
}

std::unique_ptr<TerminationCriterionParameters>
load_max_iterations_without_improvement(const toml::node& termination)
{
    const auto max_iter_wi = termination.value<unsigned long long>();
    if (!max_iter_wi) { throw InvalidNodeError{}; }
    return std::make_unique<MaxIterationsWithoutImprovementParameters>(
        *max_iter_wi);
}

}
