#include "tune/InitializerParametersLoaderTOML.hpp"

#include "tune/Error.hpp"

namespace tune {

namespace floating_point {

std::unique_ptr<PopulationInitializerParameters>
load_random_population_initializer_parameters(
    unsigned population_size,
    const ProblemParameters& problem_parameters,
    const toml::table& initializer)
{
    const auto min = initializer["range"][0].value<double>();
    if (!min) { throw InvalidNodeError{{IndexedTracePoint{"range", 0}}}; }
    const auto max = initializer["range"][1].value<double>();
    if (!max) { throw InvalidNodeError{{IndexedTracePoint{"range", 1}}}; }

    return std::make_unique<RandomPopulationInitializerParameters>(
        population_size,
        SHO::RealFunctionDomain{*min, *max},
        problem_parameters.get_domain());
}

std::unique_ptr<PopulationInitializerParameters>
load_fixed_population_initializer_parameters(
    unsigned population_size,
    const ProblemParameters& problem_parameters,
    const toml::table& initializer)
{
    std::vector<Solution> res;
    res.reserve(population_size);

    if (const auto* solutions = initializer["solutions"].as_array()) {
        if (solutions->size() != population_size) {
            const InvalidNumberOfInitialSolutionsError err{
                population_size, static_cast<unsigned>(solutions->size())};
            err.push_trace_point(BasicTracePoint{"solutions"});
            throw err;
        }
        std::size_t i = 0;
        for (const auto& solution_node : *solutions) {
            auto solution_opt = solution_node.value<Solution>();
            if (!solution_opt) {
                throw InvalidNodeError{{IndexedTracePoint{"solutions", i}}};
            }
            res.push_back(*solution_opt);
            ++i;
        }
    } else if (const auto solutions_file =
                   initializer["solutions_file"].value<std::string_view>()) {
        std::filesystem::path filepath{*solutions_file};
        std::ifstream file{filepath};
        if (!file) {
            const FileOpenError err{std::move(filepath)};
            err.push_trace_point(BasicTracePoint{"solutions_file"});
            throw err;
        }
        Solution solution;
        for (unsigned i = 0; i < population_size && file >> solution; ++i) {
            res.push_back(solution);
        }
        if (res.size() != population_size) {
            const InvalidNumberOfInitialSolutionsError err{
                population_size, static_cast<unsigned>(res.size()), filepath};
            err.push_trace_point(BasicTracePoint{"solutions_file"});
            throw err;
        }
    } else {
        throw MissingAlternativeNodeError{{"solutions", "solutions_file"}};
    }

    return std::make_unique<FixedPopulationInitializerParameters>(
        std::move(res), problem_parameters.get_domain());
}

}

namespace permutation_vector {

std::unique_ptr<PopulationInitializerParameters>
load_random_population_initializer_parameters(
    unsigned population_size,
    const ProblemParameters& problem_parameters,
    const toml::table& /*initializer*/)
{
    return std::make_unique<RandomPopulationInitializerParameters>(
        population_size, problem_parameters.get_solution_size());
}

}

}
