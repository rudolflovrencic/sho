#include "tune/ProblemParametersLoaderTOML.hpp"

#include "tune/Error.hpp"

namespace tune {

namespace floating_point {

std::unique_ptr<ProblemParameters>
load_square_problem_parameters(const toml::table& problem)
{
    const auto min = problem["domain"][0].value<double>();
    if (!min) { throw InvalidNodeError{{IndexedTracePoint{"domain", 0}}}; }
    const auto max = problem["domain"][1].value<double>();
    if (!max) { throw InvalidNodeError{{IndexedTracePoint{"domain", 1}}}; }

    return std::make_unique<SquareProblemParameters>(
        SHO::RealFunctionDomain{*min, *max});
};

}

namespace permutation_vector {

std::unique_ptr<ProblemParameters>
load_traveling_salesman_problem_parameters(const toml::table& problem)
{
    const auto tsplib_file = problem["tsplib_file"].value<std::string_view>();
    if (!tsplib_file) {
        throw InvalidNodeError{{BasicTracePoint{"tsplib_file"}}};
    }
    auto distance_matrix =
        TravelingSalesmanProblem::DistanceMatrix::load_from_tsplib_file(
            *tsplib_file);
    return std::make_unique<TravelingSalesmanProblemParameters>(
        std::move(distance_matrix));
}

}

}
