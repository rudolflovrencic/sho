#include "tune/CrossoverParametersLoaderTOML.hpp"

namespace tune {

namespace floating_point {

std::unique_ptr<CrossoverParameters> load_arithmetic_crossover_parameters(
    const ProblemParameters& /*problem_parameters*/,
    const toml::table& /*crossover*/)
{
    return std::make_unique<ArithmeticCrossoverParameters>();
}

}

namespace permutation_vector {

std::unique_ptr<CrossoverParameters>
load_cycle_crossover_parameters(const ProblemParameters& /*problem_parameters*/,
                                const toml::table& /*crossover*/)
{
    return std::make_unique<CycleCrossoverParameters>();
}

std::unique_ptr<CrossoverParameters> load_partially_mapped_crossover_parameters(
    const ProblemParameters& /*problem_parameters*/,
    const toml::table& /*crossover*/)
{
    return std::make_unique<PartiallyMappedCrossoverParameters>();
}

std::unique_ptr<CrossoverParameters>
load_order_crossover_parameters(const ProblemParameters& /*problem_parameters*/,
                                const toml::table& /*crossover*/)
{
    return std::make_unique<OrderCrossoverParameters>();
}

}

}
