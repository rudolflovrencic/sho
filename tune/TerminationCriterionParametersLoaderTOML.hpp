#ifndef SHO_TUNE_TERMINATIONCRITERIONPARAMETERSLOADERTOML_HPP
#define SHO_TUNE_TERMINATIONCRITERIONPARAMETERSLOADERTOML_HPP

#include "tune/TerminationCriterionParameters.hpp"

#include <toml++/toml.h>

namespace tune {

using TerminationCriterionParametersLoaderTOML =
    std::unique_ptr<TerminationCriterionParameters> (*)(
        const toml::node& termination);

std::unique_ptr<TerminationCriterionParameters>
load_max_running_time(const toml::node& termination);

std::unique_ptr<TerminationCriterionParameters>
load_max_evaluations(const toml::node& termination);

std::unique_ptr<TerminationCriterionParameters>
load_max_iterations(const toml::node& termination);

std::unique_ptr<TerminationCriterionParameters>
load_max_iterations_without_improvement(const toml::node& termination);

}

#endif
