#include "SHO/TerminationCriterion.hpp"

#include <sstream>
#include <cassert>

namespace SHO {

const TerminationCriterion*
check_termination_critera(const std::vector<std::reference_wrapper<TerminationCriterion>>& termination_criteria,
                          const State& state)
{
    const auto it = std::find_if(termination_criteria.cbegin(),
                                 termination_criteria.cend(),
                                 [&state](TerminationCriterion& termination_criterion) noexcept -> bool {
                                     return termination_criterion.is_met(state);
                                 });
    return it != termination_criteria.cend() ? &it->get() : nullptr;
}

MaxIterations::MaxIterations(unsigned long long max_iterations) noexcept : max_iterations{max_iterations} {}

bool MaxIterations::is_met(const State& state)
{
    return state.iteration >= max_iterations;
}

std::string MaxIterations::format_termination_message() const
{
    std::ostringstream oss;
    oss << "Maximum number of iterations (" << max_iterations << ") has been reached.";
    return oss.str();
}

unsigned long long MaxIterations::get_max_iterations() const noexcept
{
    return max_iterations;
}

MaxIterationsWithoutImprovement::MaxIterationsWithoutImprovement(
    unsigned long long max_iterations_without_improvement) noexcept
        : max_iterations_without_improvement{max_iterations_without_improvement}
{}

bool MaxIterationsWithoutImprovement::is_met(const State& state)
{
    return state.n_iterations_without_improvement >= max_iterations_without_improvement;
}

std::string MaxIterationsWithoutImprovement::format_termination_message() const
{
    std::ostringstream oss;
    oss << "Maximum number of iterations without improvement (" << max_iterations_without_improvement
        << ") has been reached.";
    return oss.str();
}

unsigned long long MaxIterationsWithoutImprovement::get_max_iterations_without_improvement() const noexcept
{
    return max_iterations_without_improvement;
}

MaxEvaluations::MaxEvaluations(unsigned long long max_evaluations) noexcept : max_evaluations{max_evaluations} {}

bool MaxEvaluations::is_met(const State& state)
{
    return state.n_evaluations >= max_evaluations;
}

std::string MaxEvaluations::format_termination_message() const
{
    std::ostringstream oss;
    oss << "Maximum number of iterations evaluations (" << max_evaluations << ") has been reached.";
    return oss.str();
}

unsigned long long MaxEvaluations::get_max_evaluations() const noexcept
{
    return max_evaluations;
}

MaxRunningTime::MaxRunningTime(std::chrono::duration<double> max_running_time) noexcept
        : max_running_time{max_running_time}
{}

bool MaxRunningTime::is_met(const State& state)
{
    return state.running_time >= max_running_time;
}

std::string MaxRunningTime::format_termination_message() const
{
    std::ostringstream oss;
    auto dur = max_running_time;
    auto h   = std::chrono::duration_cast<std::chrono::hours>(dur);
    auto m   = std::chrono::duration_cast<std::chrono::minutes>(dur -= h);
    auto s   = std::chrono::duration_cast<std::chrono::seconds>(dur -= m);
    auto ms  = std::chrono::duration_cast<std::chrono::milliseconds>(dur -= s);
    oss << "Maximum running time (";
    bool first_output{true};
    if (h > std::chrono::hours(0)) {
        oss << h.count() << "h";
        first_output = false;
    }
    if (m > std::chrono::minutes(0)) {
        if (!first_output) { oss << ' '; }
        oss << m.count() << "min";
        first_output = false;
    }
    if (s > std::chrono::seconds(0)) {
        if (!first_output) { oss << ' '; }
        oss << s.count() << "s";
        first_output = false;
    }
    if (ms > std::chrono::milliseconds(0)) {
        if (!first_output) { oss << ' '; }
        oss << ms.count() << "ms";
        first_output = false;
    }
    oss << ") has been reached.";
    return oss.str();
}

std::chrono::duration<double> MaxRunningTime::get_max_running_time() const noexcept
{
    return max_running_time;
}

}
