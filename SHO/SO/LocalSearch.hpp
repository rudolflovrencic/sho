#ifndef SHO_SO_LOCALSEARCH_HPP
#define SHO_SO_LOCALSEARCH_HPP

#include "SHO/Random.hpp"
#include "SHO/Assert.hpp"
#include "SHO/State.hpp"
#include "SHO/TerminationCriterion.hpp"
#include "SHO/NeighborhoodProvider.hpp"

#include "SHO/SO/Evaluator.hpp"
#include "SHO/SO/Individual.hpp"

namespace SHO::SO {

template<typename Solution>
class LocalSearch {
  public:
    using State                = SHO::State;
    using NeighborhoodProvider = SHO::NeighborhoodProvider<Solution>;
    using Evaluator            = SHO::SO::Evaluator<Solution>;
    using Individual           = SHO::SO::Individual<Solution>;

  public:
    using InspectionFunction =
        std::function<void(const Individual& individual, const State& state)>;

    struct Result {
        const TerminationCriterion* met_criterion;
        Individual best_individual;
        State state;
    };

  private:
    std::unique_ptr<Evaluator> evaluator;
    std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria;
    Random::ProbabilityVector<std::unique_ptr<NeighborhoodProvider>>
        neighborhood_providers;

  public:
    LocalSearch(
        std::unique_ptr<Evaluator> evaluator,
        std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria,
        Random::ProbabilityVector<std::unique_ptr<NeighborhoodProvider>>
            neighborhood_providers) noexcept;

    Result run(
        Solution initial_solution,
        InspectionFunction inspection_function =
            [](const Individual& /*Individual*/,
               const State& /*state*/) noexcept {});

  private:
    const TerminationCriterion*
    check_termination_critera(const State& state) const;

    static void update_algorithm_state(
        double& best_fitness,
        State& state,
        const std::chrono::high_resolution_clock::time_point& start_timepoint,
        const Individual& individual) noexcept;
};

template<typename Solution>
LocalSearch<Solution>::LocalSearch(
    std::unique_ptr<Evaluator> evaluator,
    std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria,
    Random::ProbabilityVector<std::unique_ptr<NeighborhoodProvider>>
        neighborhood_providers) noexcept
        : evaluator(std::move(evaluator)),
          termination_criteria(std::move(termination_criteria)),
          neighborhood_providers(std::move(neighborhood_providers))
{}

template<typename Solution>
auto LocalSearch<Solution>::run(Solution initial_solution,
                                InspectionFunction inspection_function)
    -> Result
{
    Individual individual(std::move(initial_solution));
    individual.set_fitness(evaluator->evaluate(individual.solution));

    State state{0, 0, 1, std::chrono::seconds(0)};
    double best_fitness = individual.get_fitness();

    inspection_function(individual, state);

    const auto start_timepoint = std::chrono::high_resolution_clock::now();
    const TerminationCriterion* met_criterion = nullptr;
    while (!(met_criterion = check_termination_critera(state))) {
        NeighborhoodProvider& np = *neighborhood_providers.get_random_element();
        auto neighborhood        = np.get_neighborhood(individual.solution);
        SHO_ASSERT(!neighborhood.empty(), "Neighborhood may not be empty.");
        std::vector<Individual> evaluated_neighborhood;
        evaluated_neighborhood.reserve(neighborhood.size());
        for (auto& neighbor : neighborhood) {
            evaluated_neighborhood.emplace_back(std::move(neighbor));
            auto& individual_to_evaluate = evaluated_neighborhood.back();
            individual_to_evaluate.set_fitness(
                evaluator->evaluate(individual_to_evaluate.solution));
        }

        const auto it = std::max_element(
            evaluated_neighborhood.begin(),
            evaluated_neighborhood.end(),
            [](const Individual& lhs, const Individual& rhs) noexcept {
                return lhs.get_fitness() < rhs.get_fitness();
            });

        // Stop if neighborhood contains only worse individuals.
        if (it->get_fitness() < individual.get_fitness()) { break; }

        individual = std::move(*it);

        update_algorithm_state(
            best_fitness, state, start_timepoint, individual);
        inspection_function(individual, state);
    }

    return Result{met_criterion, std::move(individual), std::move(state)};
}

template<typename Solution>
const TerminationCriterion*
LocalSearch<Solution>::check_termination_critera(const State& state) const
{
    for (auto& termination_criterion : termination_criteria) {
        if (termination_criterion->is_met(state)) {
            return termination_criterion.get();
        }
    }
    return nullptr;
}

template<typename Solution>
void LocalSearch<Solution>::update_algorithm_state(
    double& best_fitness,
    State& state,
    const std::chrono::high_resolution_clock::time_point& start_timepoint,
    const Individual& individual) noexcept
{
    state.iteration++;
    if (best_fitness >= individual.get_fitness()) {
        state.n_iterations_without_improvement++;
    } else {
        state.n_iterations_without_improvement = 0;
        best_fitness                           = individual.get_fitness();
    }
    state.n_evaluations++; // Single evaluation per iteration.
    state.running_time =
        std::chrono::high_resolution_clock::now() - start_timepoint;
}

}

#endif
