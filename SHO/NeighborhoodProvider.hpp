#ifndef SHO_NEIGHBORHOODPROVIDER_HPP
#define SHO_NEIGHBORHOODPROVIDER_HPP

#include <vector>

namespace SHO {

template<typename Solution>
class NeighborhoodProvider {
  public:
    virtual ~NeighborhoodProvider() = default;

    virtual std::vector<Solution>
    get_neighborhood(const Solution& solution) = 0;
};

}

#endif
