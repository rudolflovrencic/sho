#ifndef SHO_STATE_HPP
#define SHO_STATE_HPP

#include <chrono>

namespace SHO {

struct State {
    unsigned long long iteration;
    unsigned long long n_iterations_without_improvement;
    unsigned long long n_evaluations;
    std::chrono::duration<double> running_time;
};

}

#endif
