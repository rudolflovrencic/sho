#ifndef SHO_MO_EVALUATOR_HPP
#define SHO_MO_EVALUATOR_HPP

#include "SHO/MO/Fitness.hpp"

namespace SHO::MO {

template<typename Solution, std::size_t N>
class Evaluator {
  public:
    virtual ~Evaluator() = default;

    virtual Fitness<N> evaluate(const Solution& solution) = 0;
};

}

#endif
