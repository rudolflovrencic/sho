#ifndef SHO_MO_INDIVIDUAL_HPP
#define SHO_MO_INDIVIDUAL_HPP

#include <limits>
#include <array>
#include <algorithm>

#include "SHO/Assert.hpp"
#include "SHO/MO/Fitness.hpp"

namespace SHO::MO {

template<typename Solution, std::size_t N>
class Individual {
    static_assert(N > 1, "Number of objectives must be greater than one.");

  public:
    using Fitness = SHO::MO::Fitness<N>;

  private:
    constexpr static auto unevaluated = std::numeric_limits<double>::quiet_NaN();

  public:
    Solution solution;

  private:
    Fitness fitness;
    unsigned rank = 0;

  public:
    explicit Individual(Solution solution) noexcept;
    Individual(Solution solution, Fitness fitness) noexcept;

    bool dominates(const Individual& other) const noexcept;

    void set_fitness(Fitness fitness) noexcept;
    const Fitness& get_fitness() const noexcept;
    bool is_evaluated() const noexcept;

    void set_rank(unsigned rank) noexcept;
    unsigned get_rank() const noexcept;
    bool is_rank_set() const noexcept;
    bool is_pareto_individual() const noexcept;
};

template<typename Solution, std::size_t N>
Individual<Solution, N>::Individual(Solution solution) noexcept : solution(std::move(solution))
{
    std::fill(fitness.begin(), fitness.end(), unevaluated);
}

template<typename Solution, std::size_t N>
Individual<Solution, N>::Individual(Solution solution, Fitness fitness) noexcept
        : solution(std::move(solution)), fitness(fitness)
{
    SHO_ASSERT(std::find(this->fitness.begin(), this->fitness.end(), unevaluated) == this->fitness.end(),
               "Fitness element may not be unevaluated.");
}

template<typename Solution, std::size_t N>
bool Individual<Solution, N>::dominates(const Individual& other) const noexcept
{
    SHO_ASSERT(is_evaluated() && other.is_evaluated(), "Both individuals must be evaluated to check domination.");
    for (std::size_t i = 0; i < N; i++) {
        if (fitness[i] < other.fitness[i]) { return false; }
    }
    return true;
}

template<typename Solution, std::size_t N>
void Individual<Solution, N>::set_fitness(Fitness fitness) noexcept
{
    SHO_ASSERT(std::find(fitness.begin(), fitness.end(), unevaluated) == fitness.end(),
               "Fitness element may not be unevaluated.");
    this->fitness = fitness;
}

template<typename Solution, std::size_t N>
auto Individual<Solution, N>::get_fitness() const noexcept -> const Fitness&
{
    SHO_ASSERT(is_evaluated(), "Individual not yet evaluated.");
    return fitness;
}

template<typename Solution, std::size_t N>
bool Individual<Solution, N>::is_evaluated() const noexcept
{
    return std::find(fitness.begin(), fitness.end(), unevaluated) == fitness.end();
}

template<typename Solution, std::size_t N>
void Individual<Solution, N>::set_rank(unsigned rank) noexcept
{
    SHO_ASSERT(rank > 0, "Rank must be greater than zero.");
    this->rank = rank;
}

template<typename Solution, std::size_t N>
unsigned Individual<Solution, N>::get_rank() const noexcept
{
    SHO_ASSERT(is_rank_set(), "Rank not yet set.");
    return rank;
}

template<typename Solution, std::size_t N>
bool Individual<Solution, N>::is_rank_set() const noexcept
{
    return rank > 0;
}

template<typename Solution, std::size_t N>
bool Individual<Solution, N>::is_pareto_individual() const noexcept
{
    return rank == 1;
}

}

#endif
