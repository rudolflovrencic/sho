#ifndef SHO_MO_EXHAUSTIVESEARCH_HPP
#define SHO_MO_EXHAUSTIVESEARCH_HPP

#include "SHO/Random.hpp"
#include "SHO/Assert.hpp"
#include "SHO/State.hpp"
#include "SHO/TerminationCriterion.hpp"
#include "SHO/Advancement.hpp"

#include "SHO/MO/Evaluator.hpp"
#include "SHO/MO/Individual.hpp"

#include <chrono>

namespace SHO::MO {

template<typename Solution, std::size_t N>
class ExhaustiveSearch {
  public:
    using State       = SHO::State;
    using Advancement = SHO::Advancement<Solution>;
    using Evaluator   = SHO::MO::Evaluator<Solution, N>;
    using Individual  = SHO::MO::Individual<Solution, N>;
    using Fitness     = SHO::MO::Fitness<N>;

    struct Result {
        const TerminationCriterion* met_criterion;
        std::vector<Individual> pareto_front;
        State state;
    };

    using InspectionFunction =
        std::function<void(const std::vector<Individual>& pareto_front,
                           const Individual& current_individual,
                           const State& state)>;

  private:
    std::unique_ptr<Evaluator> evaluator;
    std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria;
    std::unique_ptr<Advancement> advancement;

  public:
    ExhaustiveSearch(
        std::unique_ptr<Evaluator> evaluator,
        std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria,
        std::unique_ptr<Advancement> advancement) noexcept;

    Result run(
        Solution initial_solution,
        InspectionFunction inspection_function =
            [](const std::vector<Individual>& /*pareto_front*/,
               const Individual& /*current_individual*/,
               const State& /*state*/) noexcept {});

  private:
    const TerminationCriterion*
    check_termination_critera(const State& state) const;

    static void update_algorithm_state(
        std::size_t& pareto_front_size,
        State& state,
        const std::chrono::high_resolution_clock::time_point& start_timepoint,
        const std::vector<Individual>& pareto_front) noexcept;
};

template<typename Solution, std::size_t N>
ExhaustiveSearch<Solution, N>::ExhaustiveSearch(
    std::unique_ptr<Evaluator> evaluator,
    std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria,
    std::unique_ptr<Advancement> advancement) noexcept
        : evaluator(std::move(evaluator)),
          termination_criteria(std::move(termination_criteria)),
          advancement(std::move(advancement))
{}

template<typename Solution, std::size_t N>
auto ExhaustiveSearch<Solution, N>::run(Solution initial_solution,
                                        InspectionFunction inspection_function)
    -> Result
{
    Individual individual(std::move(initial_solution));
    individual.set_fitness(evaluator->evaluate(individual.solution));

    std::vector<Individual> pareto_front;
    static constexpr std::size_t reserve_size = 256;
    pareto_front.reserve(reserve_size);
    pareto_front.push_back(individual);

    State state{0, 0, 1, std::chrono::seconds(0)};
    std::size_t pareto_front_size = 1;

    inspection_function(pareto_front, individual, state);

    const auto start_timepoint = std::chrono::high_resolution_clock::now();
    const TerminationCriterion* met_criterion = nullptr;
    while (!(met_criterion = check_termination_critera(state)) &&
           advancement->advance(individual.solution)) {
        individual.set_fitness(evaluator->evaluate(individual.solution));

        bool non_dominated = true;
        for (const auto& i : pareto_front) {
            if (i.dominates(individual)) {
                non_dominated = false;
                break;
            }
        }

        if (non_dominated) {
            pareto_front.erase(
                std::remove_if(pareto_front.begin(),
                               pareto_front.end(),
                               [&individual](const Individual& i) noexcept {
                                   return individual.dominates(i);
                               }),
                pareto_front.end());
            pareto_front.push_back(individual);
        }

        update_algorithm_state(
            pareto_front_size, state, start_timepoint, pareto_front);

        inspection_function(pareto_front, individual, state);
    }

    return Result{met_criterion, std::move(pareto_front), std::move(state)};
}

template<typename Solution, std::size_t N>
auto ExhaustiveSearch<Solution, N>::check_termination_critera(
    const State& state) const -> const TerminationCriterion*
{
    for (auto& termination_criterion : termination_criteria) {
        if (termination_criterion->is_met(state)) {
            return termination_criterion.get();
        }
    }
    return nullptr;
}

template<typename Solution, std::size_t N>
void ExhaustiveSearch<Solution, N>::update_algorithm_state(
    std::size_t& pareto_front_size,
    State& state,
    const std::chrono::high_resolution_clock::time_point& start_timepoint,
    const std::vector<Individual>& pareto_front) noexcept
{
    state.iteration++;
    if (pareto_front_size == pareto_front.size()) {
        state.n_iterations_without_improvement++;
    } else {
        state.n_iterations_without_improvement = 0;
        pareto_front_size                      = pareto_front.size();
    }
    state.n_evaluations++; // Single evaluation per iteration.
    state.running_time =
        std::chrono::high_resolution_clock::now() - start_timepoint;
}

}

#endif
