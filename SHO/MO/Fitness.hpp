#ifndef SHO_MO_FITNESS_HPP
#define SHO_MO_FITNESS_HPP

#include "SHO/FunctionDomain.hpp"

#include <array>
#include <ostream>

namespace SHO::MO {

template<std::size_t N>
using Fitness = std::array<double, N>;

template<std::size_t N>
constexpr bool operator==(const Fitness<N>& lhs, const Fitness<N>& rhs) noexcept
{
    static_assert(N > 1, "Number of objectives must be at least 2.");
    for (std::size_t i = 0; i < N; i++) {
        if (lhs[i] != rhs[i]) { return false; }
    }
    return true;
}

template<std::size_t N>
constexpr bool operator!=(const Fitness<N>& lhs, const Fitness<N>& rhs) noexcept
{
    return !(lhs == rhs);
}

template<std::size_t N>
std::ostream& operator<<(std::ostream& os, const Fitness<N>& fitness)
{
    static_assert(N > 1, "Number of objectives must be at least 2.");
    os << '{';
    for (std::size_t i = 0; i < N - 1; i++) { os << fitness[i] << ", "; }
    return os << fitness.back() << '}';
}

template<std::size_t N>
using FitnessDomain = std::array<RealFunctionDomain, N>;

template<std::size_t N>
std::ostream& operator<<(std::ostream& os, const FitnessDomain<N>& domain)
{
    static_assert(N > 1, "Number of objectives must be at least 2.");
    os << '{';
    for (std::size_t i = 0; i < N - 1; i++) { os << domain[i] << ", "; }
    return os << domain.back() << '}';
}

}

template<std::size_t N>
struct std::hash<SHO::MO::Fitness<N>> {
    std::size_t operator()(const SHO::MO::Fitness<N>& fitness) const
    {
        std::hash<std::size_t> hasher;
        std::size_t res = 0;
        for (const auto& f : fitness) { res += hasher(f); }
        return res;
    }
};

#endif
