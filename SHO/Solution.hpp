#ifndef SOLUTION_HPP
#define SOLUTION_HPP

#include "SHO/Crossover.hpp"
#include "SHO/Mutation.hpp"
#include "SHO/Advancement.hpp"
#include "SHO/NeighborhoodProvider.hpp"

#include "SHO/FunctionDomain.hpp"

#include <ostream>
#include <random>
#include <vector>
#include <optional>

namespace SHO {

namespace FloatingPoint {

using Solution = double;

class ArithmeticCrossover : public Crossover<Solution> {
  public:
    double mate(const Solution& first_parent, const Solution& second_parent) override;
};

class RandomMutation : public Mutation<Solution> {
  private:
    std::uniform_real_distribution<Solution> dist;

  public:
    RandomMutation(Solution lower_bound, Solution upper_bound);

    void mutate(Solution& solution) override;
};

// FIXME: The mutation should clamp the solution to the function domain.
class GaussianMutation : public Mutation<Solution> {
  private:
    double standard_deviation;

  public:
    explicit GaussianMutation(double standard_deviation) noexcept;

    void mutate(double& solution) override;
};

class PositiveAdvancement final : public Advancement<Solution> {
  private:
    double delta;
    RealFunctionDomain domain;

  public:
    PositiveAdvancement(double delta, RealFunctionDomain domain) noexcept;

    [[nodiscard]] bool operator()(Solution& solution) final;
};

class SimpleNeighborhoodProvider : public NeighborhoodProvider<Solution> {
  public:
    std::vector<double> get_neighborhood(const double& solution) override;
};

}

namespace BinaryVector {

using Solution = std::vector<bool>;

class UniformCrossover : public Crossover<Solution> {
  public:
    Solution mate(const Solution& first_parent, const Solution& second_parent) override;
};

class SinglePointCrossover : public Crossover<Solution> {
  public:
    Solution mate(const Solution& first_parent, const Solution& second_parent) override;
};

class RandomMutation : public Mutation<Solution> {
  public:
    void mutate(Solution& solution) override;
};

class RandomFlipMutation : public Mutation<Solution> {
  public:
    void mutate(Solution& solution) override;
};

}

namespace PermutationVector {

using Element  = unsigned;
using Solution = std::vector<Element>; // TODO: Consider using array instead.

class CycleCrossover : public Crossover<Solution> {
  private:
    std::vector<std::optional<Element>> child_data;

  public:
    Solution mate(const Solution& first_parent, const Solution& second_parent) override;
};

class TwoPointCrossover : public Crossover<Solution> {
  public:
    using CrossoverPoints = std::pair<std::size_t, std::size_t>;

  public:
    Solution mate(const Solution& first_parent, const Solution& second_parent) final;

  private:
    virtual Solution
    mate(const Solution& first_parent, const Solution& second_parent, CrossoverPoints crossover_points) = 0;

    static CrossoverPoints get_crossover_points(std::size_t size) noexcept;
};

class PartiallyMappedCrossover : public TwoPointCrossover {
  public:
    Solution second_parent_buffer;

  public:
    Solution
    mate(const Solution& first_parent, const Solution& second_parent, CrossoverPoints crossover_points) override;
};

class OrderCrossover : public TwoPointCrossover {
  public:
    Solution
    mate(const Solution& first_parent, const Solution& second_parent, CrossoverPoints crossover_points) override;
};

class SwapMutation : public Mutation<Solution> {
  public:
    void mutate(Solution& solution) override;
};

class RandomMutation : public Mutation<Solution> {
  public:
    void mutate(Solution& solution) override;
};

class SwapNeighborhoodProvider : public NeighborhoodProvider<Solution> {
  public:
    std::vector<Solution> get_neighborhood(const Solution& solution) override;
};

}

}

std::ostream& operator<<(std::ostream& os, const std::vector<bool>& vec);
std::ostream& operator<<(std::ostream& os, const std::vector<unsigned>& s);

#endif
