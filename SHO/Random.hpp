#ifndef SHO_RANDOM_HPP
#define SHO_RANDOM_HPP

#include "Assert.hpp"

#include <algorithm>
#include <vector>
#include <memory>
#include <random>
#include <type_traits>

namespace SHO::Random {

std::mt19937& get_generator() noexcept;

template<typename T>
class ProbabilityVector {
  private:
    std::vector<T> elements;
    std::discrete_distribution<std::size_t> distribution;

  public:
    ProbabilityVector(std::vector<T> elements,
                      const std::vector<double>& weights) noexcept;

    std::size_t size() const noexcept;

    T& get_random_element() noexcept;
};

template<typename T>
ProbabilityVector<T>::ProbabilityVector(
    std::vector<T> elements, const std::vector<double>& weights) noexcept
        : elements(std::move(elements)),
          distribution(weights.begin(), weights.end())
{
    SHO_ASSERT(!this->elements.empty(),
               "Cannot create probability vector with no elements.");
    SHO_ASSERT_FMT(
        this->elements.size() == weights.size(),
        "Number of elements ({}) not equal to the number of weights ({}).",
        this->elements.size(),
        weights.size());
}

template<typename T>
std::size_t ProbabilityVector<T>::size() const noexcept
{
    return elements.size();
}

template<typename T>
T& ProbabilityVector<T>::get_random_element() noexcept
{
    return elements[distribution(get_generator())];
}

}

#endif
