#ifndef SHO_MUTATION_HPP
#define SHO_MUTATION_HPP

namespace SHO {

template<typename Solution>
class Mutation {
  public:
    virtual ~Mutation() = default;

    virtual void mutate(Solution& solution) = 0;
};

}

#endif
