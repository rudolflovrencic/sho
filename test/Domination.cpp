#include <catch2/catch_test_macros.hpp>

#include "SHO/MO/Individual.hpp"
#include "SHO/MO/Population.hpp"

TEST_CASE("Domination")
{
    constexpr static std::size_t n_objectives = 2;
    using Individual = SHO::MO::Individual<int, n_objectives>;

    Individual i1(1, {-1, -50});
    Individual i2(2, {-3, -20});
    REQUIRE(!i1.dominates(i2));
    REQUIRE(!i2.dominates(i1));

    Individual i3(3, {-4, -30});
    REQUIRE(!i1.dominates(i3));
    REQUIRE(i2.dominates(i3));
    REQUIRE(!i3.dominates(i1));
    REQUIRE(!i3.dominates(i2));

    Individual i4(4, {-5, -30});
    REQUIRE(!i1.dominates(i4));
    REQUIRE(i2.dominates(i4));
    REQUIRE(i3.dominates(i4));
    REQUIRE(!i4.dominates(i1));
    REQUIRE(!i4.dominates(i2));
    REQUIRE(!i4.dominates(i3));
}

TEST_CASE("NondominatedSorting")
{
    constexpr static std::size_t n_objectives = 2;
    using Population = SHO::MO::Population<int, n_objectives>;

    Population population;
    population.reserve(8);

    population.push_back({1, {-1, -50}});
    population.push_back({3, {-2, -30}});
    population.push_back({4, {-3, -20}});
    population.push_back({5, {-6, -10}});

    population.push_back({2, {-2, -40}});
    population.push_back({6, {-4, -30}});
    population.push_back({7, {-5, -20}});

    population.push_back({8, {-5, -30}});

    population.nondominated_sort();

    for (std::size_t i = 0; i < 4; i++) {
        REQUIRE(population[i].get_rank() == 1);
    }
    for (std::size_t i = 4; i < 7; i++) {
        REQUIRE(population[i].get_rank() == 2);
    }
    REQUIRE(population[7].get_rank() == 3);
}
